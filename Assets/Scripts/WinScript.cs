﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WinFinish());
    }


    private IEnumerator WinFinish() 
    {
        yield return new WaitForSeconds(3.0f);
        FindObjectOfType<PauseMenu>().Restart();
    }    
}
