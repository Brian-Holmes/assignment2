﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class coinSpawnScript : MonoBehaviour
{

    Vector3 coinHeightOffset = new Vector3(0.0f, 0.75f, 0.0f);
    
    [SerializeField]
    public GameObject coinPrefab;
    public GameObject prefab_spawnIndicator;
    public bool needsCoin;
    public bool needsCoinSpawn;
    public bool removeIndicator;
    
    void Start()
    {
        needsCoin = false;
        needsCoinSpawn = true;
        removeIndicator = false;
    }
        
    void Update()
    {
        if (needsCoinSpawn)
        {
            CreateSpawnPoint();
            needsCoinSpawn = false;
        }

        if (needsCoin)
        {
            CreateCoin();
            needsCoin = false;            
        }

        if (removeIndicator)
        {
            RemoveIndicator();
            removeIndicator = false;
        }
    }

    public void QueueCoinSpawn()
    {
        StartCoroutine(makeCoinSpawn());
    }

    private IEnumerator makeCoinSpawn()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(8.0f, 15.0f));
        needsCoinSpawn = true;
    }

    private void CreateSpawnPoint()
    {
        var spawnIndicator = Instantiate(prefab_spawnIndicator, transform.position, transform.rotation);
        spawnIndicator.transform.parent = gameObject.transform;
        QueueCoin();
    }


    public void QueueCoin()
    {
        StartCoroutine(makeCoin());     
    }
    private IEnumerator makeCoin()
    {
        yield return new WaitForSeconds(3.0f);
        needsCoin = true;
    }
    private void CreateCoin() 
    {
        var newCoin = Instantiate(coinPrefab, transform.position + coinHeightOffset, transform.rotation);
        newCoin.transform.parent = gameObject.transform;        
        StartCoroutine(deleteIndicator());
    }


    private IEnumerator deleteIndicator()
    {
        yield return new WaitForSeconds(1.5f);
        removeIndicator = true;
    }
    private void RemoveIndicator()
    {
        Destroy(GetComponent<ParticleSystem>());       
    }




}
