﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;

    public GameObject pauseMenuUI;
    public GameObject winMenuUI;
    public GameObject loseMenuUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }        
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
    public void Win()
    {
        winMenuUI.SetActive(true);       
    }
    public void Restart()
    {
        winMenuUI.SetActive(false);        
    }
    public void Lose()
    {
        loseMenuUI.SetActive(true);
    }
    public void Replay()
    {
        loseMenuUI.SetActive(false);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");       
    }
    
    public void QuitGame()
    {
        Debug.Log("Quitting Game....");
        Application.Quit();
    }


}
