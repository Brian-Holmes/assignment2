﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankScript : MonoBehaviour
{
    GameObject player;
    bool inTheBank = false;
    public Material bankColor;
    public Material depositingColor;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        
    }

 
    void Update()
    {
        if (player.GetComponent<ScoreScript>().bankedCoins >= 20)
        {
            Debug.Log(">>> 20 Coins In Bank...");
            FindObjectOfType<PauseMenu>().Win();
            StartCoroutine(Win());
        }
        if (player.GetComponent<ScoreScript>().lives < 0)
        {
            Debug.Log(">>> 20 Coins In Bank...");
            player.GetComponent<ScoreScript>().EndGame();
        }
    }

    private IEnumerator Win() 
    {
        yield return new WaitForSeconds(5.0f);
        FindObjectOfType<PauseMenu>().winMenuUI.SetActive(false);
        player.GetComponent<ScoreScript>().WinGame();
    }

    

    private void OnTriggerEnter(Collider other)
    {
        inTheBank = true;
        Debug.Log(">>> In Bank...");
        if (other.gameObject.tag == "Player" && player.GetComponent<ScoreScript>().coins > 0)
        {
            Debug.Log(">>> Banking Coins...");
            StartCoroutine(bankCoin(player.GetComponent<ScoreScript>().coins));
        }
    }

    private void OnTriggerExit(Collider other)
    {        
        if (other.gameObject.tag == "Player")
        {
            Debug.Log(">>> Left Bank...");
            inTheBank = false;
        }
    }

       
    private IEnumerator bankCoin(int c)
    {
        //for (int i = 0; i < c ; i++)
        //{
        while (inTheBank) 
            {
            if (player.GetComponent<ScoreScript>().coins > 0 && player.GetComponent<ScoreScript>().bankedCoins < 20)
            {
                yield return new WaitForSeconds(0.5f);
                player.GetComponent<ScoreScript>().coins--;
                player.GetComponent<ScoreScript>().bankedCoins++;
                player.GetComponent<ScoreScript>().PlayBankSound();
                GetComponent<MeshRenderer>().material = depositingColor;
                yield return new WaitForSeconds(0.25f);
                GetComponent<MeshRenderer>().material = bankColor;
            }
            else 
            {
                inTheBank = false;
            }
                

            }
            
      //  }      
    }

}
