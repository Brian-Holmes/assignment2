﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;
using UnityEngine.UIElements;

public class ScoreScript : MonoBehaviour
{
    public int lives;
    private int startLives;
    public int coins;
    public int prevCoins;
    public int bankedCoins;
    public GUIStyle pocketStyle;
    public GUIStyle bankStyle;
    //public GUIStyle gameStyle;

    public AudioSource coinDropAudio;
    public AudioClip pickup;
    public AudioClip drop;
    public AudioClip bank;
    public AudioClip fall;
    //public AudioClip music;

    public void Start()
    {
        startLives = lives;
        Debug.Log("Lives: " + startLives  + "\n");
        lives = startLives;
        coinDropAudio = GetComponent<AudioSource>();

        coins = 0;
        prevCoins = coins;
        bankedCoins = 0;
    }

    public void WinGame() 
    {        
        Debug.Log("You Win!!!");
        EndGame();
    }
    public void EndGame() 
    {       
        lives = startLives;
        bankedCoins = 0;
        FindObjectOfType<PauseMenu>().LoadMenu();
    }

    public void PlayPickupSound()
    {
        coinDropAudio.PlayOneShot(pickup);
        Debug.Log("PlayPickupSound");
    }
    public void PlayBankSound()
    {
        coinDropAudio.PlayOneShot(bank);
        Debug.Log("PlayBankSound");
    }
    public void PlayDropSound()
    {
        coinDropAudio.PlayOneShot(drop);
        Debug.Log("PlayDropSound");
    }
    public void PlayFallSound()
    {
        coinDropAudio.PlayOneShot(fall);
        Debug.Log("PlayFallSound");
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(15, 450, 200, 35), "Lives remaining : " + lives, pocketStyle);
        GUI.Label(new Rect(15, 500, 200, 35), "Pocket : " + coins + " coins", pocketStyle);
        GUI.Label(new Rect(15, 550, 200, 35), "Bank : " + bankedCoins + " coins", bankStyle);
    }
}
