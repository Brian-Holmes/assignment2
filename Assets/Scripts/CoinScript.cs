﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    Vector3 spin = new Vector3(0.0f, 1f, 0.0f);
    public float speed = 100f;
    public AudioSource coinPickupAudio;


    void Start()
    {

        coinPickupAudio = GetComponent<AudioSource>();


    }

    void Update()
    {
        this.transform.Rotate(spin * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            // add a coin to coins
            coinPickupAudio.Play();
            other.GetComponent<ScoreScript>().coins++;
            other.GetComponent<ScoreScript>().PlayPickupSound();
            // queue next spawn of coin at this location
            transform.parent.gameObject.GetComponent<coinSpawnScript>().QueueCoinSpawn();
            //Pickup coin
            Destroy(gameObject);
        }
    }


    public void CoinDropped()
    {
        StartCoroutine(CleanUpCoins());
    }
    private IEnumerator CleanUpCoins() 
    {
        yield return new WaitForSeconds(5.0f);
        Destroy(gameObject);
    }


}
