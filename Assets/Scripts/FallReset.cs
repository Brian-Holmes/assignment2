﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityStandardAssets.Characters.ThirdPerson;

public class FallReset : MonoBehaviour {

    private Vector3 home;	


	// Use this for initialization
	void Start () {
        home = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -1f) {
			transform.position = home;
			GetComponent<ScoreScript>().coins = 0;
			GetComponent<ScoreScript>().PlayFallSound();
			if (GetComponent<ScoreScript>().lives > 0)
			{
				GetComponent<ScoreScript>().lives--;
			}

			if (GetComponent<ScoreScript>().lives <= 0)
			{
				FindObjectOfType<PauseMenu>().Lose();
				StartCoroutine(Lose());
			}
		}		
	}


	private IEnumerator Lose()
	{
		yield return new WaitForSeconds(5.0f);
		FindObjectOfType<PauseMenu>().loseMenuUI.SetActive(false);
		GetComponent<ScoreScript>().EndGame();
	}
}
