﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class BulletController : MonoBehaviour {
    public float speed = 1f;

    private GameObject player;
    [SerializeField]
    public GameObject coinPrefab;
    Vector3 coinHeightOffset = new Vector3(0.0f, 2.0f, 0.0f);
    public float coinBounce = 25.0f;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 pos = transform.position;
        Vector3 direction = player.transform.position - transform.position;
        direction.y += 0.5f;
        direction = Vector3.Normalize(direction) * speed / 100f;
        pos = pos + direction;
        transform.position = pos;
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == player && collision.gameObject.GetComponent<ScoreScript>().coins > 0)
        {
            collision.gameObject.GetComponent<ScoreScript>().coins--;
            collision.gameObject.GetComponent<ScoreScript>().PlayDropSound();
            // drop a coin
            DropCoin();

        }
        Debug.Log("Coins after hit: " + collision.gameObject.GetComponent<ScoreScript>().coins);
        Destroy(this.gameObject);
    }

    private void DropCoin()
    {
        var newCoin = Instantiate(coinPrefab, transform.position + coinHeightOffset, transform.rotation);
        newCoin.gameObject.transform.Rotate(new Vector3(0.0f, 20.0f, -15.0f));
        newCoin.AddComponent<Rigidbody>();
        Rigidbody rb = newCoin.GetComponent<Rigidbody>();
        //newCoin.AddComponent<Collider>();
        Collider col = newCoin.GetComponent<Collider>();
        col.isTrigger = false;
        rb.AddRelativeForce(new Vector3(100.0f, 100.0f, -150.0f));
        newCoin.GetComponent<CoinScript>().CoinDropped();
    }
}
