﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class WaterScript : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<ThirdPersonCharacter>().ToggleWater();
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<ThirdPersonCharacter>().ToggleWater();
    }

}
